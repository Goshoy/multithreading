public class Test {

    public static void main(String[] args) throws InterruptedException {

        Student student = new Student();

        Thread thread1 = new Thread() {
            public void run() {
                System.out.println("get lock");
                student.changeName("Ivan");
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("release lock");
            }
        };

        Thread thread2 = new Thread() {
            public void run() {
                System.out.println("get lock");
                student.changeName("Gosho");
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("release lock");
            }
        };

        thread1.start();
        thread2.start();

        Thread.sleep(5000);
        System.out.println(student.name);

        Thread.sleep(7000);
        System.out.println(student.name);

        Thread.sleep(12000);
        System.out.println(student.name);

    }
}
