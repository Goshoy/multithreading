package blockingqueue;

import java.util.concurrent.atomic.AtomicInteger;

public class Producer implements Runnable {
    AtomicInteger counter;
    BlockingQueue queue;

    public Producer(BlockingQueue queue, AtomicInteger counter) {
        this.queue = queue;
        this.counter = counter;
    }

    @Override
    public void run() {
        while (counter.getAndAdd(0) < queue.getCapacity()) {
            try {
                this.queue.enqueue(counter.getAndIncrement());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
