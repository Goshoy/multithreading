package blockingqueue;

import java.util.concurrent.atomic.AtomicInteger;

public class Consumer implements Runnable {
    private AtomicInteger counter;
    private BlockingQueue queue;

    public Consumer(BlockingQueue queue, AtomicInteger counter) {
        this.queue = queue;
        this.counter = counter;
    }

    public void run() {
        while (counter.getAndIncrement() < queue.getCapacity()) {
            try {
                queue.dequeue();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
