package blockingqueue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class MeasurementTool {
    BlockingQueue queue;
    private int producers;
    private int consumers;
    private int limit;

    public MeasurementTool(int producers, int consumers, int limit) {
        this.producers = producers;
        this.consumers = consumers;
        this.limit = limit;
        this.queue = new BlockingQueue(this.limit);
    }

    public long measureWithExecutor() throws InterruptedException {
        AtomicInteger counter = new AtomicInteger(0);
        AtomicInteger counter1 = new AtomicInteger(0);

        ExecutorService executor = Executors.newFixedThreadPool(4);

        for (int i = 0; i < this.producers; i++) {
            executor.execute(new Producer(queue, counter1));
        }

        for (int i = 0; i < this.consumers; i++) {
            executor.execute(new Consumer(queue, counter));
        }

        long startTime = System.currentTimeMillis();

        executor.shutdown();

        executor.awaitTermination(5, TimeUnit.SECONDS);

        long endTime = System.currentTimeMillis();

        return (endTime - startTime);
    }

    public long measure() throws InterruptedException {
        List<Thread> producers = new ArrayList<>();
        List<Thread> consumers = new ArrayList<>();
        AtomicInteger counter = new AtomicInteger(0);
        AtomicInteger counter1 = new AtomicInteger(0);

        for (int i = 0; i < this.producers; i++) {
            producers.add(new Thread(new Producer(queue, counter1)));
        }

        for (int i = 0; i < this.consumers; i++) {
            consumers.add(new Thread(new Consumer(queue, counter)));
        }

        long startTime = System.currentTimeMillis();

        producers.stream().forEach(t -> t.start());
        consumers.stream().forEach(t -> t.start());

        producers.stream().forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        consumers.stream().forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        long endTime = System.currentTimeMillis();

        return (endTime - startTime);
    }

    public static void main(String[] args) throws InterruptedException {
        MeasurementTool tool = new MeasurementTool(2, 2, 2_000_000);

        System.out.println(tool.measure() + " blocking queue size " + tool.queue.getSize());
    }
}


