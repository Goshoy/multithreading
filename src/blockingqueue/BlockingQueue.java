package blockingqueue;

public class BlockingQueue {
    private final int capacity;
    private int front, rear;
    private int size = 0;
    private int[] array = null;
    private int steps = 0;

    public BlockingQueue(int capacity) {
        if (capacity > 0) {
            this.capacity = capacity;
        } else {
            this.capacity = 16;
        }

        this.array = new int[this.capacity];
        this.front = 0;
        this.rear = this.capacity - 1;
    }

    public int getCapacity() {
        return this.capacity;
    }

    public int getSize() {
        return this.size;
    }

    public boolean isFull() {
        return this.size == capacity;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public synchronized void enqueue(int element) throws InterruptedException {
        while (this.isFull()) {
            wait();
        }

        this.rear = (this.rear + 1) % this.capacity;
        this.array[this.rear] = element;
        this.size++;
        this.steps++;

        notifyAll();
    }

    public synchronized int dequeue() throws InterruptedException {
        while (this.isEmpty()) {
            wait();
        }

        int element = this.array[this.front];
        this.front = (this.front + 1) % this.capacity;
        this.size--;
        this.steps++;

        notifyAll();

        return element;
    }
}
