package atomicinteger;

import java.util.concurrent.atomic.AtomicInteger;

public class TestAtomicInteger {

    public static void main(String[] args) {

        AtomicInteger number = new AtomicInteger(0);

        long startTime = System.currentTimeMillis();

        while (number.get() < 100000000) {
            number.getAndIncrement();
        }

        long endTime = System.currentTimeMillis();

        System.out.println(endTime - startTime);
        System.out.println(number.get());

    }
}
