package atomicinteger;

public class MyAtomicInteger {
    private int value;
    atomicinteger.Lock lock;

    public MyAtomicInteger(int initial) {
        this.value = initial;
        this.lock = new Lock();

    }

    // Why is their function not synchronized, do they use locks?
    // My code is slower with the lock though...
    // How is it that fast, still 2 times faster?
    public synchronized int getAndIncrement() throws InterruptedException {
        // lock.lock();

        int current = this.value;
        this.value++;

        // lock.unlock();

        return current;
    }

    public int getAndDecrement() throws InterruptedException {
        lock.lock();
        int current = this.value;
        this.value--;

        lock.unlock();

        return current;
    }

    public int get() {
        return this.value;
    }

}
