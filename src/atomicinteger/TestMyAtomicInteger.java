package atomicinteger;

public class TestMyAtomicInteger {

    public static void main(String[] args) {
        MyAtomicInteger number = new MyAtomicInteger(0);

        long startTime = System.currentTimeMillis();

        while (number.get() < 100000000) {
            try {
                number.getAndIncrement();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        long endTime = System.currentTimeMillis();

        System.out.println(endTime - startTime);
        System.out.println(number.get());

    }
}
