public class WaitTest {

    private int stringLength = 0;
    Student student = new Student();

    public synchronized void findLenght() throws InterruptedException {
        System.out.println("findLenght method gets lock");
        while (student.name == null) {
            Thread.sleep(2000);
            System.out.println(Thread.currentThread().getName() + " is in while!");

            Thread.sleep(2000);
            System.out.println("findLock method releases lock");
            wait();
            Thread.sleep(2000);
            System.out.println("findLenght method gets lock");
        }
        Thread.sleep(2000);
        stringLength = student.name.length();
        System.out.println(student.name + " : " + student.name.length());
        System.out.println("findLock method releases lock");
        Thread.sleep(2000);
    }

    public synchronized void changeName(String name) throws InterruptedException {
        System.out.println("changeName method gets lock");
        Thread.sleep(2000);
        System.out.println(
                Thread.currentThread().getName() + " is changing the name of the student!");

        Thread.sleep(2000);
        this.student.name = name;
        System.out.println("Name changed -> " + student.name);
        Thread.sleep(2000);
        notifyAll();
        System.out.println("changeName method releases lock");
        Thread.sleep(2000);
    }

    public static void main(String[] args) throws InterruptedException {
        WaitTest test = new WaitTest();

        Thread thread1 = new Thread() {
            public void run() {
                try {
                    test.findLenght();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread thread2 = new Thread() {
            public void run() {
                try {
                    test.changeName("Gosho");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        thread1.start();
        thread2.start();
    }

}
