package processnumbers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;



// In this program the adding of the elements is slow!!
// The stream api part is fast!


public class ProcessStreamAPI {
    List<Integer> numbers;

    public ProcessStreamAPI() {
        this.numbers = new ArrayList<>();
    }

    public int init() {
        int counter = 0;
        Random rand = new Random();

        for (int i = 0; i < 150000000; i++) {
            int random = rand.nextInt(1000000);
            this.numbers.add(random);
            if (random % 2 == 0) {
                counter++;
            }
        }

        return counter;
    }

    public static void main(String[] args) {
        ProcessStreamAPI process = new ProcessStreamAPI();
        long counter = process.init();

        long startTime = System.currentTimeMillis();

        long counter2 =
                process.numbers.parallelStream().filter(integer -> (integer % 2 == 0)).count();

        long endTime = System.currentTimeMillis();
        System.out.println(endTime - startTime);
        System.out.println(counter2);
        System.out.println(counter);
    }

}
