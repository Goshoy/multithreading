package processnumbers;

public class MyRunnable implements Runnable {

    int rear;
    int front;
    int[] arr;
    int counter = 0;

    public MyRunnable(int[] arr, int rear, int front) {
        this.rear = rear;
        this.front = front;
        this.arr = arr;
    }

    @Override
    public synchronized void run() {
        for (int i = this.rear; i < this.front; i++) {
            if (this.arr[i] % 2 == 0) {
                this.counter++;
            }
        }
    }
}
