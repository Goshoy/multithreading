package processnumbers;

import java.util.Random;

public class ProcessSingleThreaded {
    int[] numbers;

    public ProcessSingleThreaded() {
        this.numbers = new int[150000000];
    }

    public int init() {
        int counter = 0;
        Random rand = new Random();

        for (int i = 0; i < this.numbers.length; i++) {
            this.numbers[i] = rand.nextInt(1000000);
            if (this.numbers[i] % 2 == 0) {
                counter++;
            }
        }

        return counter;
    }

    public static void main(String[] args) throws InterruptedException {
        ProcessSingleThreaded process = new ProcessSingleThreaded();
        int numberOfEven = process.init();
        MyRunnable runnable = new MyRunnable(process.numbers, 0, 150000000);
        Thread thread = new Thread(runnable);

        long startTime = System.currentTimeMillis();

        thread.start();
        thread.join();

        long endTime = System.currentTimeMillis();
        System.out.println((endTime - startTime));
        System.out.println(runnable.counter);
        System.out.println(numberOfEven);
    }
}
