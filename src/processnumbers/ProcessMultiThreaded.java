package processnumbers;

import java.util.Random;

public class ProcessMultiThreaded {
    int[] numbers;

    public ProcessMultiThreaded() {
        this.numbers = new int[150000000];
    }

    public int init() {
        int counter = 0;
        Random rand = new Random();

        for (int i = 0; i < this.numbers.length; i++) {
            this.numbers[i] = rand.nextInt(1000000);
            if (this.numbers[i] % 2 == 0) {
                counter++;
            }
        }

        return counter;
    }


    public static void main(String[] args) throws InterruptedException {
        ProcessMultiThreaded process = new ProcessMultiThreaded();
        int numberOfEven = process.init();

        MyRunnable runnable1 = new MyRunnable(process.numbers, 0, 25000000);
        MyRunnable runnable2 = new MyRunnable(process.numbers, 25000000, 50000000);
        MyRunnable runnable3 = new MyRunnable(process.numbers, 50000000, 100000000);
        MyRunnable runnable4 = new MyRunnable(process.numbers, 100000000, 125000000);
        MyRunnable runnable5 = new MyRunnable(process.numbers, 125000000, 150000000);


        Thread thread1 = new Thread(runnable1);
        Thread thread2 = new Thread(runnable2);
        Thread thread3 = new Thread(runnable3);
        Thread thread4 = new Thread(runnable4);
        Thread thread5 = new Thread(runnable5);

        long startTime = System.currentTimeMillis();

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();

        thread1.join();
        thread2.join();
        thread3.join();
        thread4.join();
        thread5.join();


        long endTime = System.currentTimeMillis();
        System.out.println((endTime - startTime));
        System.out.println(runnable1.counter + runnable2.counter + runnable3.counter
                + runnable4.counter + runnable5.counter);
        System.out.println(numberOfEven);
    }
}
